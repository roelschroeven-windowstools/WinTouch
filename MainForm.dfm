object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'WinTouch'
  ClientHeight = 224
  ClientWidth = 644
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  DesignSize = (
    644
    224)
  PixelsPerInch = 96
  TextHeight = 13
  object LabelFile: TLabel
    Left = 8
    Top = 8
    Width = 628
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 93
    Height = 13
    Caption = 'Current timestamp:'
  end
  object LabelCurrentTimestamp: TLabel
    Left = 40
    Top = 59
    Width = 10
    Height = 16
    Caption = '--'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 96
    Width = 77
    Height = 13
    Caption = 'New timestamp:'
  end
  object MaskEditNewTimestamp: TMaskEdit
    Left = 40
    Top = 115
    Width = 152
    Height = 24
    EditMask = '####-##-## ##:##:##;1;_'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 19
    ParentFont = False
    TabOrder = 0
    Text = '    -  -     :  :  '
  end
  object ButtonSetToFileTime: TButton
    Left = 216
    Top = 114
    Width = 75
    Height = 25
    Caption = '&File time'
    TabOrder = 1
    OnClick = ButtonSetToFileTimeClick
  end
  object ButtonSetToNow: TButton
    Left = 297
    Top = 114
    Width = 75
    Height = 25
    Caption = '&Now'
    TabOrder = 2
    OnClick = ButtonSetToNowClick
  end
  object ButtonTouch: TButton
    Left = 8
    Top = 160
    Width = 628
    Height = 56
    Anchors = [akLeft, akTop, akRight]
    Caption = '&Touch'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = ButtonTouchClick
  end
  object MainMenu: TMainMenu
    Left = 496
    Top = 40
    object File1: TMenuItem
      Caption = 'File'
      object Open1: TMenuItem
        Caption = 'Open ...'
        ShortCut = 16463
        OnClick = Open1Click
      end
      object Exit1: TMenuItem
        Caption = 'E&xit'
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object About1: TMenuItem
        Caption = 'About'
        OnClick = About1Click
      end
    end
  end
  object ApplicationEvents: TApplicationEvents
    OnIdle = ApplicationEventsIdle
    Left = 544
    Top = 40
  end
  object FileOpenDialog: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = [fdoPathMustExist, fdoFileMustExist]
    Title = 'Select file'
    Left = 496
    Top = 96
  end
end
