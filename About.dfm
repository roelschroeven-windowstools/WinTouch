object FormAbout: TFormAbout
  Left = 0
  Top = 0
  BorderWidth = 5
  Caption = 'About WinTouch'
  ClientHeight = 291
  ClientWidth = 747
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnKeyDown = FormKeyDown
  DesignSize = (
    747
    291)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 250
    Height = 13
    Caption = 'Author: Roel Schroeven <roel@roelschroeven.net>'
  end
  object Label2: TLabel
    Left = 8
    Top = 50
    Width = 39
    Height = 13
    Caption = 'License:'
  end
  object LinkLabelIcons8Finger: TLinkLabel
    Left = 8
    Top = 27
    Width = 259
    Height = 17
    Caption = 
      'Thanks to <a href="https://icons8.com/">Icons8</a> for the icon:' +
      ' <a href="https://icons8.com/web-app/17669/One-Finger">One finge' +
      'r icon credits</a>'
    TabOrder = 0
    OnLinkClick = LinkLabelIcons8FingerLinkClick
  end
  object MemoLicense: TMemo
    Left = 8
    Top = 69
    Width = 731
    Height = 212
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      'The MIT License (MIT)'
      ''
      'Copyright (c) 2016 Roel Schroeven <roel@roelschroeven.net>'
      ''
      
        'Permission is hereby granted, free of charge, to any person obta' +
        'ining a copy of this software and associated documentation files' +
        ' (the "Software"), '
      
        'to deal in the Software without restriction, including without l' +
        'imitation the rights to use, copy, modify, merge, publish, distr' +
        'ibute, sublicense, and/or '
      
        'sell copies of the Software, and to permit persons to whom the S' +
        'oftware is furnished to do so, subject to the following conditio' +
        'ns:'
      ''
      
        'The above copyright notice and this permission notice shall be i' +
        'ncluded in all copies or substantial portions of the Software.'
      ''
      
        'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, ' +
        'EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE '
      
        'WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE ' +
        'AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR '
      
        'COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIAB' +
        'ILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR '
      
        'OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWA' +
        'RE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.')
    ReadOnly = True
    TabOrder = 1
    ExplicitWidth = 609
    ExplicitHeight = 220
  end
end
