//---------------------------------------------------------------------------

#ifndef AboutH
#define AboutH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#pragma message "End of auto-included header files"
//---------------------------------------------------------------------------

void ShowAbout();
//---------------------------------------------------------------------------

class TFormAbout : public TForm
{
__published:	// IDE-managed Components
  TLabel *Label1;
  TLinkLabel *LinkLabelIcons8Finger;
  TLabel *Label2;
  TMemo *MemoLicense;
  void __fastcall LinkLabelIcons8FingerLinkClick(TObject *Sender, const UnicodeString Link,
          TSysLinkType LinkType);
  void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
private:	// User declarations
public:		// User declarations
  __fastcall TFormAbout(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormAbout *FormAbout;
//---------------------------------------------------------------------------
#endif
