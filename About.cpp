//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "About.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormAbout *FormAbout;
//---------------------------------------------------------------------------

void ShowAbout()
{
  TFormAbout *Form;
  Application->CreateForm(__classid(TFormAbout), &Form);
  Form->ShowModal();
  delete Form;
}
//---------------------------------------------------------------------------

__fastcall TFormAbout::TFormAbout(TComponent* Owner)
  : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFormAbout::LinkLabelIcons8FingerLinkClick(TObject *Sender, const UnicodeString Link,
          TSysLinkType LinkType)
{
  ShellExecuteW(NULL, L"open", Link.c_str(), NULL, NULL, SW_SHOWNORMAL);
}
//---------------------------------------------------------------------------

void __fastcall TFormAbout::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
  if (Key == VK_ESCAPE)
    Close();
}
//---------------------------------------------------------------------------
