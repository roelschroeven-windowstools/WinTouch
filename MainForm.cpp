//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainForm.h"
#include "About.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormMain *FormMain;
//---------------------------------------------------------------------------

TDateTime GetFileTime(const String &Fn)
{
  HANDLE hFile = CreateFileW(
    Fn.c_str(),
    GENERIC_READ,
    FILE_SHARE_READ,
    NULL,
    OPEN_EXISTING,
    0,
    NULL);
  if (!hFile)
    throw Exception(L"Failed to open file");
  FILETIME LastWriteTime;
  BOOL rv = GetFileTime(hFile, NULL, NULL, &LastWriteTime);
  CloseHandle(hFile);
  if (!rv)
    throw Exception(L"Failed to get file time");

  SYSTEMTIME FileSystemTimeUtc;
  FileTimeToSystemTime(&LastWriteTime, &FileSystemTimeUtc);
  SYSTEMTIME FileSystemTimeLocal;
  SystemTimeToTzSpecificLocalTime(NULL, &FileSystemTimeUtc, &FileSystemTimeLocal);
  return SystemTimeToDateTime(FileSystemTimeLocal);
}

void SetFileTime(const String &Fn, TDateTime Timestamp)
{
  SYSTEMTIME FileSystemTimeLocal;
  DateTimeToSystemTime(Timestamp, FileSystemTimeLocal);
  SYSTEMTIME FileSystemTimeUtc;
  TzSpecificLocalTimeToSystemTime(NULL, &FileSystemTimeLocal, &FileSystemTimeUtc);
  FILETIME LastWriteTime;
  SystemTimeToFileTime(&FileSystemTimeUtc, &LastWriteTime);

  HANDLE hFile = CreateFileW(
    Fn.c_str(),
    FILE_WRITE_ATTRIBUTES,
    FILE_SHARE_WRITE,
    NULL,
    OPEN_EXISTING,
    0,
    NULL);
  if (!hFile)
    throw Exception(L"Failed to open file");
  BOOL rv = SetFileTime(hFile, NULL, NULL, &LastWriteTime);
  CloseHandle(hFile);
  if (!rv)
    throw Exception(L"Failed to set file time");
}

String FormatTimestamp(TDateTime Timestamp)
{
  return Timestamp.FormatString(L"yyyy-mm-dd' 'hh:nn:ss");
}

TDateTime ParseTimestamp(const String &s)
{
  int Year = s.SubString(1, 4).ToIntDef(-1);
  int Month = s.SubString(6, 2).ToIntDef(-1);
  int Day = s.SubString(9, 2).ToIntDef(-1);
  int Hour = s.SubString(12, 2).ToIntDef(-1);
  int Minute = s.SubString(15, 2).ToIntDef(-1);
  int Second = s.SubString(18, 2).ToIntDef(-1);
  return TDate(Year, Month, Day) + TTime(Hour, Minute, Second, 0);
}
//---------------------------------------------------------------------------

// TODO: link to icons8: <a href="https://icons8.com/web-app/17669/One-Finger">One finger icon credits</a>
__fastcall TFormMain::TFormMain(TComponent* Owner)
	: TForm(Owner)
  , m_bFirstIdle(true)
{
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ApplicationEventsIdle(TObject *Sender, bool &Done)
{
  if (m_bFirstIdle)
    {
    m_bFirstIdle = false;
    if (!LoadFile())
      Close();
    }
}
//---------------------------------------------------------------------------

bool TFormMain::LoadFile()
{
  if (ParamCount() >= 1)
    {
    LoadFile(ParamStr(1));
    return true;
    }
  else
    {
    if (!FileOpenDialog->Execute())
      return false;
    LoadFile(FileOpenDialog->FileName);
    return true;
    }
}
//---------------------------------------------------------------------------

void TFormMain::LoadFile(const String &Fn)
{
  LabelFile->Caption = Fn;
  TDateTime Timestamp = GetFileTime(Fn);
  String TimestampString = FormatTimestamp(Timestamp);
  LabelCurrentTimestamp->Caption = TimestampString;
  MaskEditNewTimestamp->Text = TimestampString;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::Open1Click(TObject *Sender)
{
  if (FileOpenDialog->Execute())
    LoadFile(FileOpenDialog->FileName);
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
  if (Key == VK_ESCAPE)
    Close();
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonSetToFileTimeClick(TObject *Sender)
{
  MaskEditNewTimestamp->Text = LabelCurrentTimestamp->Caption;
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonSetToNowClick(TObject *Sender)
{
  MaskEditNewTimestamp->Text = FormatTimestamp(Now());
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::ButtonTouchClick(TObject *Sender)
{
  SetFileTime(LabelFile->Caption, ParseTimestamp(MaskEditNewTimestamp->Text));
  LabelCurrentTimestamp->Caption = FormatTimestamp(GetFileTime(LabelFile->Caption));
}
//---------------------------------------------------------------------------

void __fastcall TFormMain::About1Click(TObject *Sender)
{
  ShowAbout();
}
//---------------------------------------------------------------------------

