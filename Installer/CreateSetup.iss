#define SOURCE_DIR="..\App"
#define VERSION GetFileVersion(AddBackslash(SOURCE_DIR) + "WinTouch.exe")

[Setup]
SourceDir={#SOURCE_DIR}
SolidCompression=true
Compression=lzma
VersionInfoVersion={#VERSION}
AllowNoIcons=yes
AppName=WinTouch
AppVerName=WinTouch {#VERSION}
DefaultDirName={pf}\WinTouch
DefaultGroupName=WinTouch
SetupIconFile=..\WinTouch_Icon.ico
AppID={{8e939eb9-e96f-4629-97c4-b1e16c65b729}
OutputDir=..\Installer
OutputBaseFilename=WinTouchSetup
VersionInfoCompany=Roel Schroeven
VersionInfoDescription=GUI for changing file date/time
VersionInfoTextVersion={#VERSION}
VersionInfoCopyright=(c) Roel Schroeven
AppPublisher=Roel Schroeven
AppContact=Roel Schroeven <roel@roelschroeven.net>
AppVersion={#VERSION}
[Files]
Source: WinTouch.exe; DestDir: {app}; Flags: touch
[Icons]
Name: {group}\WinTouch; Filename: {app}\WinTouch.exe
[Registry]
Root: HKLM; Subkey: "Software\Classes\*\shell\WinTouch"; Flags: uninsdeletekey
Root: HKLM; Subkey: "Software\Classes\*\shell\WinTouch\command"; ValueName: ""; ValueType: string; ValueData: """{app}\WinTouch.exe"" %1"; Flags: uninsdeletekey
