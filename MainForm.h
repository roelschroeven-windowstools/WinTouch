//---------------------------------------------------------------------------

#ifndef MainFormH
#define MainFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Mask.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.AppEvnts.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TFormMain : public TForm
{
__published:	// IDE-managed Components
	TMainMenu *MainMenu;
	TMenuItem *File1;
	TMenuItem *Open1;
	TMenuItem *Exit1;
	TMenuItem *Help1;
	TMenuItem *About1;
  TLabel *LabelFile;
	TLabel *Label2;
	TLabel *LabelCurrentTimestamp;
	TLabel *Label4;
	TMaskEdit *MaskEditNewTimestamp;
  TButton *ButtonSetToFileTime;
  TButton *ButtonSetToNow;
  TButton *ButtonTouch;
	TApplicationEvents *ApplicationEvents;
  TFileOpenDialog *FileOpenDialog;
	void __fastcall ApplicationEventsIdle(TObject *Sender, bool &Done);
  void __fastcall Open1Click(TObject *Sender);
  void __fastcall FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift);
  void __fastcall ButtonSetToFileTimeClick(TObject *Sender);
  void __fastcall ButtonSetToNowClick(TObject *Sender);
  void __fastcall ButtonTouchClick(TObject *Sender);
  void __fastcall About1Click(TObject *Sender);

private:	// User declarations
  bool m_bFirstIdle;
  bool LoadFile();
  void LoadFile(const String &Fn);

public:		// User declarations
	__fastcall TFormMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFormMain *FormMain;
//---------------------------------------------------------------------------
#endif
